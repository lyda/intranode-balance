package ie.deri.hadoop;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.Long;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.TreeSet;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.DF;
import org.apache.hadoop.fs.Path;

class Block implements Comparable<Block> {
  private String meta_path, blk_path;
  private long meta_size, blk_size;

  Block(File meta) throws IOException {
    meta_path = meta.getPath();
    meta_size = meta.length();
    Pattern p = Pattern.compile("(/.*/blk_[+-]?\\d+)_\\d+\\.meta");
    Matcher m = p.matcher(meta_path);
    if (m.matches()) {
      blk_path = m.group(1);
      blk_size = new File(blk_path).length();
    } else {
      throw new IOException("Block filename for '" + meta_path + "' not found.");
    }
  }

  public boolean equals(Block other) {
    return this.toString() == other.toString();
  }

  public int hashCode() {
    return meta_path.hashCode();
  }

  public int compareTo(Block other) {
    if (this.size() > other.size())
      return 1;
    if (this.size() < other.size())
      return -1;
    return this.toString().compareTo(other.toString());
  }

  public String toString() {
    return new String(meta_path + " (" + meta_size + ")  " + blk_path + " (" + blk_size + ")");
  }

  public long size() {
    return meta_size + blk_size;
  }

  public void move(String src, String dest) throws IOException {
    String dest_meta_path = dest + meta_path.substring(src.length(), meta_path.length());
    String dest_blk_path = dest + blk_path.substring(src.length(), blk_path.length());
    FileUtils.moveFile(new File(meta_path), new File(dest_meta_path));
    FileUtils.moveFile(new File(blk_path), new File(dest_blk_path));
    meta_path = dest_meta_path;
    blk_path = dest_blk_path;
  }
}


class Partition implements Comparable<Partition> {
  public String path;
  public long size;
  public TreeSet<Block> blocks = new TreeSet<Block>();

  public Partition(String path) throws IOException {
    this.path = path;
    this.refresh();
    for (File meta: FileUtils.listFiles(new File(path, "current"), new String[] {"meta"}, true)) {
      try {
        Block block = new Block(meta);
        blocks.add(block);
      } catch (IOException e) {
        System.out.println("Error on block " + e.getMessage());
      }
    }
  }

  public void refresh() throws IOException {
    // Set interval to 0 because we want to know the current state.
    DF df = new DF(new File(path), 0);
    size = df.getAvailable();
  }

  public boolean equals(Partition other) {
    return path == other.path;
  }

  public int hashCode() {
    return path.hashCode();
  }

  public int compareTo(Partition other) {
    if (size > other.size)
      return 1;
    if (size < other.size)
      return -1;
    return path.compareTo(other.path);
  }

  public String toString() {
    return new String(path + " (" + size + ")");
  }
}


class DiskState {
  public ArrayList<Partition> partition_list = null;
  public TreeSet<Partition> partitions = null;

  DiskState(String[] dfs_data_dirs) throws IOException {
    partition_list = new ArrayList<Partition>();
    for (String path: dfs_data_dirs) {
      partition_list.add(new Partition(path));
    }
    this.refresh();
  }

  public void refresh() {
    partitions = new TreeSet<Partition>(partition_list);
  }

  public long difference() {
    return partitions.last().size - partitions.first().size;
  }

  public long largestBlockOnCrampedPartition() {
    return partitions.first().blocks.last().size();
  }

  public void move() throws IOException {
    Partition spacious = partitions.last();
    Partition cramped = partitions.first();

    Block block = cramped.blocks.last();
    cramped.blocks.remove(block);

    try {
      System.out.print("Moving from " + block);
      block.move(cramped.path, spacious.path);
      System.out.println(" to " + block);
    } catch (IOException e) {
      System.out.println("Error moving block " + e.getMessage());
      System.exit(1);
    }

    spacious.refresh();
    cramped.refresh();
    this.refresh();
  }

}


public class IntraNodeBalancer {
  public static void main(String[] args) throws IOException {
    Configuration conf = new Configuration();
    String[] prop_files = {"hdfs-default.xml", "hdfs-site.xml"};
    if (args.length == 1) {
      for (String prop_file: prop_files) {
        System.out.println("Loading " + args[0] + "/" + prop_file);
        conf.addResource(new Path(args[0] + "/" + prop_file));
      }
    } else {
      for (String prop_file: prop_files) {
        System.out.println("Loading " + prop_file + " from classpath.");
        Configuration.addDefaultResource(prop_file);
      }
    }

    String dfs_data_dir = conf.get("dfs.data.dir");
    if (dfs_data_dir == null) {
      System.out.println("Can't find the dfs.data.dir property.");
      System.exit(1);
    }

    String[] dfs_data_dirs = dfs_data_dir.split(",");
    if (dfs_data_dirs.length <= 1) {
      System.out.println("The dfs.data.dir property must have more than one directory defined.");
      System.exit(1);
    }

    DiskState ds = new DiskState(dfs_data_dirs);

    while (ds.largestBlockOnCrampedPartition() < (ds.difference() / 2)) {
      ds.move();
    }
    System.out.println("All done now.");
  }
}
