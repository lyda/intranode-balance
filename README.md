# Intranode balancer

This tool requires that (at least) the node being balanced should
have the DataNode process down. It's probably best to take down the
cluster to stop hadoop from trying to make sure block replica
minimums are met.

# How to run

This is built specifically against version 1.0.3 of hadoop. It
expects `../hadoop-common` will contain a built version of
hadoop. I'm not really good with Java so the `build.xml` file is
kind of brittle this way.

The easiest way to run it is to just use `ant`:

    ant run -Dargs=/path/to/hadoop/config

Do that as the hadoop user and it will move the blocks around.

There is no dry run option. If it fails in the middle of moving a
block, it will be unhappy.
